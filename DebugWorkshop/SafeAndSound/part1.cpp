#include "part1.h"
#include <iostream>

char* string_copy(char* dest, unsigned int destsize, char* src)
{
	char* ret = dest;
	int i = 0;

	for (i = 0; i < destsize; i++)
	{
		dest[i] = '\0';
	}
	for (i = 0; i < strlen(src); i++)
	{
		dest[i] = src[i];
		std::cout << src[i] << std::endl;
	}
	

	return ret;
}

void part1()
{
	char password[] = "secret";
	char dest[12];
	char src[] = "hello world!";

	string_copy(dest, 12, src);

	std::cout << src << std::endl;
	std::cout << dest << std::endl;
}
